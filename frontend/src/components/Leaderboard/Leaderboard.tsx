import React, { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import Navbar from "../Navbar/Navbar";

import "bootstrap/dist/css/bootstrap.min.css";
import "./Leaderboard.css";

interface Score {
  id: string;
  username: string;
  correct_answers: number;
}

function Leaderboard() {
  // State to store the list of scores
  const [scores, setScores] = useState<Score[]>([]);

  // Fetch scores from the server when the component mounts
  useEffect(() => {
    fetch("http://0.0.0.0:8000/")
      .then((response) => response.json())
      .then((data) => {
        // Sort scores in descending order based on correct_answers
        const sortedScores = data.data.sort(function (a: any, b: any) {
          return b.correct_answers - a.correct_answers;
        });
        // Update the scores state with the sorted scores
        setScores(sortedScores);
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, []);

  return (
    <Navbar>
      <div className="bg">
        <div className="leaderboard-bg">
          <h2 className="leaderboard-heading">Leaderboard</h2>
          {/* Display a table to show leaderboard */}
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Rank</th>
                <th>Username</th>
                <th>Score</th>
              </tr>
            </thead>
            <tbody>
              {/* Map through scores to display each score */}
              {scores.map((score, index) => (
                <tr key={score.id}>
                  <td>{index + 1}</td> {/* Display the rank */}
                  <td>{score.username}</td> {/* Display the username */}
                  <td>{score.correct_answers}</td> {/* Display the score */}
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    </Navbar>
  );
}

export default Leaderboard;
