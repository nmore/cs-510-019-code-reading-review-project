import * as React from "react";
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useAuth0 } from "@auth0/auth0-react";
import Navbar from "../Navbar/Navbar";

import "bootstrap/dist/css/bootstrap.min.css";
import "./Home.css";

function Home() {
  // Get authentication and navigation functions from the Auth0 hook
  const { loginWithRedirect, user, isAuthenticated } = useAuth0();

  // Initialize navigation hook
  const navigate = useNavigate();

  // Handle the "Let's Play" button click
  const handlePlayGame = () => {
    // Check if the user is authenticated and redirect accordingly
    if (user !== undefined && isAuthenticated) {
      console.log("Redirecting to the game screen...");
      navigate("/game");
    } else {
      console.log("Redirecting to the login screen...");
      loginWithRedirect();
    }
  };

  return (
    <Navbar>
      <div className="bg">
        <div className="home-bg">
          <h2 className="home-heading">Full Stack Web Development Trivia</h2>
          {/* Button to start the game */}
          <Button variant="primary" id="start-game" onClick={handlePlayGame}>
            LET&apos;S PLAY
          </Button>
        </div>
      </div>
    </Navbar>
  );
}

export default Home;
