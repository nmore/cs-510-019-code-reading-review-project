import * as React from "react";
import { Button, Dropdown } from "react-bootstrap";
import { useAuth0 } from "@auth0/auth0-react";

import "bootstrap/dist/css/bootstrap.min.css";
import "./Navbar.css";

// Props type for the Navbar component
type ContainerProps = {
  children: React.ReactNode;
};

// Navbar component
const Navbar = (props: ContainerProps) => {
  // Get authentication functions and user details from Auth0 hook
  const { loginWithRedirect, logout, user, isAuthenticated } = useAuth0();

  return (
    <div className="navbar-container">
      <div id="user-details-container">
        {/* Display login and signup buttons when user is not authenticated */}
        {user == undefined && !isAuthenticated && (
          <div>
            <Button
              id="login-button"
              variant="secondary"
              onClick={() => loginWithRedirect()}
            >
              LOG IN
            </Button>
            <Button id="signin-button">SIGN UP</Button>
          </div>
        )}
        {/* Display user details and dropdown when user is authenticated */}
        {user !== undefined && isAuthenticated && (
          <Dropdown id="user-details">
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              <img src={user.picture} alt={user.name} />
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item>{user.name}</Dropdown.Item>
              <Dropdown.Item href="/leaderboard">Leaderboard</Dropdown.Item>
              <Dropdown.Item
                id="dropdown-logout"
                onClick={() =>
                  logout({ logoutParams: { returnTo: window.location.origin } })
                }
              >
                Log Out
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        )}
      </div>

      {props.children} {/* Render child components */}
    </div>
  );
};

export default Navbar;
