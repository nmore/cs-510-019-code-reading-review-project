import React, { useEffect, useState } from "react";
import { Button, Card, Spinner } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useAuth0 } from "@auth0/auth0-react";

import "bootstrap/dist/css/bootstrap.min.css";
import "./GameScreen.css";

import Navbar from "../Navbar/Navbar";

function QuizComponent() {
  // Initialize authentication and navigation hooks
  const { user } = useAuth0();
  const navigate = useNavigate();

  // State variables to manage quiz state
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [quizQuestions, setQuizQuestions] = useState([
    {
      question: "question",
      options: {
        1: "option1",
        2: "option2",
        3: "option3",
      },
      answer: 1,
    },
  ]);

  // Get the current question based on the current index
  const currentQuestion = quizQuestions[currentQuestionIndex];

  // State variables for loading and game status
  const [showLoading, setShowLoading] = useState(false);
  const [showGameScreen, setShowGameScreen] = useState(true);
  const [showEndScreen, setShowEndScreen] = useState(false);
  const [selected, setSelected] = useState<number[]>([]);
  const [finalScore, setFinalScore] = useState(0);

  // Fetch quiz questions from the server on component mount
  useEffect(() => {
    fetch("/question/list")
      .then((response) => response.json())
      .then((data) => {
        setQuizQuestions(data.questionList);
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, []);

  // Send user's score to the server
  const postScore = async (score: number) => {
    if (user !== undefined) {
      await fetch("http://0.0.0.0:8000/", {
        method: "post",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          username: user.name,
          correct_answers: score,
          date: new Date().toISOString().slice(0, 10),
        }),
      })
        .then((response) => {
          console.log(response);
        })
        .catch((err) => {
          console.log(err.message);
        });
    }
  };

  // Calculate and update the user's final score
  const calculateScore = () => {
    setShowLoading(false);
    const matchingCount = selected.reduce((count, value, index) => {
      const { answer } = quizQuestions[index];
      if (Number(value) === answer) {
        return count + 1;
      }
      return count;
    }, 0);

    finishGame(matchingCount);
  };

  // Finish the game and show the end screen
  const finishGame = (matchingCount: number) => {
    postScore(matchingCount);
    setFinalScore(matchingCount);
    setShowEndScreen(true);
  };

  // Handle click event for the next question button
  const handleNextQuestion = (event: any) => {
    setSelected((oldArray) => [...oldArray, event.target.value]);
    if (currentQuestionIndex === quizQuestions.length - 1) {
      setShowLoading(true);
      setShowGameScreen(false);
    } else {
      setCurrentQuestionIndex((prevIndex) => prevIndex + 1);
    }
  };

  // Calculate the final score when all questions are answered
  useEffect(() => {
    if (selected.length === 5) {
      calculateScore();
    }
  }, [selected]);

  // Handle click event to navigate to the leaderboard
  const handleOpenLeaderboard = () => {
    navigate("/leaderboard");
  };

  // Render the quiz component
  return (
    <Navbar>
      <div className="bg">
        {showLoading && (
          <div className="spinner-container">
            <Spinner animation="border" role="status">
              <span className="visually-hidden">Loading...</span>
            </Spinner>
          </div>
        )}
        {!showLoading && showGameScreen && (
          <div className="game-bg">
            <Card className="game-card">
              <Card.Body>
                <div className="trivia-card">
                  <Card>
                    <Card.Header>{currentQuestion.question}</Card.Header>
                    <Card.Body>
                      <Card.Text>
                        {Object.entries(currentQuestion.options).map(
                          ([optionKey, optionValue]) => (
                            <Button
                              key={optionKey}
                              className="mt-4"
                              id="option-btn"
                              variant="primary"
                              onClick={handleNextQuestion}
                              value={optionKey}
                            >
                              {optionValue}
                            </Button>
                          )
                        )}
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </div>
              </Card.Body>
            </Card>
          </div>
        )}
        {showEndScreen && (
          <div className="game-finish-container">
            <h2 className="heading">Good Job!</h2>
            <h4 className="sub-heading">
              you got {finalScore} out of 5 correct
            </h4>
            <div>
              <Button
                variant="primary"
                className="mt-4"
                id="open-leaderboard"
                onClick={handleOpenLeaderboard}
              >
                LEADERBOARD
              </Button>
            </div>
          </div>
        )}
      </div>
    </Navbar>
  );
}

export default QuizComponent;
