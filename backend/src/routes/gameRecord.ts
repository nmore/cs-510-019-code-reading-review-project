const mongoose = require("mongoose");
import express, { Request, Response } from "express";
const router = express.Router();

const Question = require("../models/Question");

// Route to test if the server is up
router.get("/", (req: Request, res: Response) => {
  res.send("Hello game!");
});

// Display all questions
router.get("/list", (req: Request, res: Response) => {
  // Find all questions from the database
  Question.find()
    .exec()
    .then((result: string) => {
      res.status(201).json({
        message: "List all questions",
        questionList: result,
      });
    })
    .catch((err: Error) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

// Create a new question
router.post("/create", (req: Request, res: Response) => {
  // Extract question details from the request body
  const question = new Question({
    _id: new mongoose.Types.ObjectId(),
    question: req.body.question,
    options: req.body.options,
    answer: req.body.answer,
  });

  // Save the new question to the database
  question
    .save()
    .then((result: string) => {
      console.log(result);
      res.status(201).json({
        message: "Document created!",
        questionCreated: result,
      });
    })
    .catch((err: Error) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

// Read a specific question by its ID
router.get("/:questionId", (req: Request, res: Response) => {
  const id = req.params.questionId;

  // Find the question by ID
  Question.findById(id)
    .exec()
    .then((result: string) => {
      if (result) {
        res.status(200).json({
          message: "Document fetched!",
          question: result,
        });
      }
    })
    .catch(() => {
      res.status(404).json({ message: "No valid entry found for provided ID" });
    });
});

// Update a question by its ID
router.patch("/:questionId", (req: Request, res: Response) => {
  const id = req.params.questionId;

  console.log(req.body);
  // Find and update the question by ID
  Question.findOneAndUpdate({ _id: id }, req.body)
    .exec()
    .then((result: string) => {
      res.status(200).json({
        message: "Document updated!",
        questionUpdated: result,
      });
    })
    .catch((err: Error) => {
      res.status(500).json({
        error: err,
      });
    });
});

// Delete a question by its ID
router.delete("/:questionId", (req: Request, res: Response) => {
  const id = req.params.questionId;

  // Find and delete the question by ID
  Question.findOneAndDelete({ _id: id })
    .exec()
    .then((result: string) => {
      res.status(200).json({
        message: "Document deleted!",
        questionDeleted: result,
      });
    })
    .catch((err: Error) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

module.exports = router;
